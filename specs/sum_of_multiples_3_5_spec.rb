require 'rspec'
require_relative 'spec_helper'
require_relative '../katas/sum_of_multiples_3_5'

describe 'solve the problem' do

  it 'will give the sum of the number given' do
    expect(sum_multiples(10)).to eq 23
    expect(sum_multiples(20)).to eq 78
    expect(sum_multiples(1000)).to eq 233168
  end

end