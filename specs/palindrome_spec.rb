require_relative 'spec_helper'
require_relative '../katas/palindrome'

describe 'is_palindrome?' do
  it 'should return true given a palindrome' do
    expect(is_palindrome? 'racecar').to be true
    expect(is_palindrome? 'tacocat').to be true
    expect(is_palindrome? 'ABBA').to be true
  end

  it 'should return false given a non-palindrome' do
    expect(is_palindrome? 'racecat').to be false
    expect(is_palindrome? 'tacocar').to be false
    expect(is_palindrome? 'palindrome').to be false
  end
end