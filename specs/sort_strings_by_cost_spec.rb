require_relative 'spec_helper'
require_relative '../katas/sort_strings_by_cost'

# testing the solution
describe 'sort strings by cost' do
  it 'should sort strings according to cost' do
    expect(sort_strings_by_cost(%w(tem cab exz))).to eq(%w(cab tem exz))
    expect(sort_strings_by_cost(%w(az bc hi jk))).to eq(%w(bc hi jk az))
    expect(sort_strings_by_cost(%w(az bc cb jk))).to include('az','bc','cb','jk')
  end
end