require_relative 'spec_helper'
require_relative '../katas/prime_numbers'

describe 'primes' do
  it 'should find all the prime numbers between 1 and 10' do
    expect(primes 10).to match_array([2, 3, 5, 7])
  end

  it 'should find all the prime numbers between 1 and 50' do
    expect(primes 50).to match_array([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47])
  end

  it 'should find all the prime numbers between 1 and 100' do
    expect(primes 100).to match_array([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97])
  end

  it 'should find all the prime numbers between 1 and 200' do
    expect(primes 200).to match_array([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199])
  end
end