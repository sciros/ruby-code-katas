require 'rspec'
require_relative 'spec_helper'
require_relative '../katas/fibonacci_summation'

describe 'sum the even values of fibonacci given an upper threshold' do

  it 'will sum small sequences correctly' do
    expect(sum_even_fibonacci(4)).to eq 2
    expect(sum_even_fibonacci(10)).to eq 10
    expect(sum_even_fibonacci(40)).to eq 44
    expect(sum_even_fibonacci(90)).to eq 44
  end

  it 'will solve for 4 million' do
    expect(sum_even_fibonacci(4000000)).to eq 4613732
  end

end