require_relative 'spec_helper'
require_relative '../katas/triangle'

describe 'triangle' do
  it 'should return equilateral if all 3 sides are equal' do
    expect(triangle(4,4,4)).to eq :equilateral
  end

  it 'should return isosceles if only 2 sides are equal' do
    expect(triangle(7,4,4)).to eq :isosceles
    expect(triangle(4,4,7)).to eq :isosceles
    expect(triangle(4,7,4)).to eq :isosceles
  end

  it 'should return scalene if no sides are equal' do
    expect(triangle(4,5,6)).to eq :scalene
  end
end


describe 'is_valid?' do
  it 'should return true given valid sides for a triangle' do
    expect(is_valid? 3,4,5).to be true
    expect(is_valid? 5,5,5).to be true
    expect(is_valid? 5,5,2).to be true
    expect(is_valid? 21,11,11).to be true
  end

  it 'should return false given invalid sides for a triangle' do
    expect(is_valid? 21,10,11).to be false
    expect(is_valid? 5,5,10).to be false
  end
end