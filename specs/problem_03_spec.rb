require_relative 'spec_helper'
require_relative '../katas/problem_03'

describe 'sum the values of fibonacci given an upper threshold' do

  before(:each) do
    @fracker = PrimeFactorFinder.new
  end

  it 'will find factors for given example' do
    expect(@fracker.get_prime_factors(13195)).to match_array [5, 7, 13, 29]
  end

  it 'will find factors for really large numbers' do
    expect(@fracker.get_prime_factors(343904919586319)).to match_array [7, 13, 37, 41, 59, 67, 73, 89, 97]
  end

  it 'will find factors for the solution' do
    expect(@fracker.get_prime_factors(600851475143)).to match_array [71, 839, 1471, 6857]
  end

end