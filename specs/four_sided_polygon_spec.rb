require_relative 'spec_helper'
require_relative '../katas/four_sided_polygon'

# testing the solution
describe 'classify_polygon' do
  it 'should return square given 4 equal sides and 4 equal angles' do
    expect(classify_polygon([2,2,2,2],[90,90,90,90])).to eq('square')
  end

  it 'should return rhombus given 4 equal sides but not equal angles' do
    expect(classify_polygon([2,2,2,2],[80,100,80,100])).to eq('rhombus')
  end

  it 'should return rectangle given 4 equal angles but not equal sides' do
    expect(classify_polygon([2,2,4,4],[90,90,90,90])).to eq('rectangle')
  end

  it 'should return parallelogram given 2 pairs of equal angles and 2 pairs of equal sides' do
    expect(classify_polygon([2,2,4,4],[80,80,100,100])).to eq('parallelogram')
  end
end