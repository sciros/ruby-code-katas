require_relative 'spec_helper'
require_relative '../katas/roman_numerals'

describe 'to_roman' do
  it 'should return I given 1' do
    expect(to_roman 1).to eq 'I'
  end

  it 'should return II given 2' do
    expect(to_roman 2).to eq 'II'
  end

  it 'should return IV given 4' do
    expect(to_roman 4).to eq 'IV'
  end

  it 'should return VIII given 8' do
    expect(to_roman 8).to eq 'VIII'
  end

  it 'should return IX given 9' do
    expect(to_roman 9).to eq 'IX'
  end

  it 'should return XIV give 14' do
    expect(to_roman 14).to eq 'XIV'
  end

  it 'should return MCMXCVIII given 1998' do
    expect(to_roman 1998).to eq 'MCMXCVIII'
  end
end

describe 'to_decimal' do
  it 'should return 1 given I' do
    expect(to_decimal 'I').to be 1
  end

  it 'should return 2 given II' do
    expect(to_decimal 'II').to be 2
  end

  it 'should return 4 given IV' do
    expect(to_decimal 'IV').to be 4
  end

  it 'should return 10 given X' do
    expect(to_decimal 'X').to be 10
  end

  it 'should return 19 given XIX' do
    expect(to_decimal 'XIX').to be 19
  end

  it 'should return 1998 given MCMXCVIII' do
    expect(to_decimal 'MCMXCVIII').to be 1998
  end
end