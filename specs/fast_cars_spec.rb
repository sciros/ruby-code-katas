require 'yaml'

require_relative 'spec_helper'
require_relative '../katas/fast_cars'

describe 'fast cars' do
  before(:each) do
    @cars = YAML.load_file('../data/cars.yml')
  end

  describe 'five_fastest_cars' do
    it 'should return the five fastest cars in the list, ordered by 0-60 time ascending' do
      expect(five_fastest_cars @cars).to eql([
                                                 'Porsche 918 Spyder',
                                                 'Bugatti Chiron',
                                                 'Porsche 911 Turbo S',
                                                 'Lamborghini Aventador LP750-4 SV',
                                                 'Ferrari 812 Superfast'
                                             ])
    end
  end

  describe 'five_highest_hp' do
    it 'should return the five highest horsepowers in the list, in descending order' do
      expect(five_highest_hp @cars).to eql([1500, 887, 789, 740, 666])
    end
  end

  describe 'best_power_to_weight' do
    it 'should return the five cars in the list with the best power to weight ratio, descending' do
      expect(best_power_to_weight @cars).to eql([
                                                    'Bugatti Chiron',
                                                    'Porsche 918 Spyder',
                                                    'Ferrari 812 Superfast',
                                                    'McLaren 675LT',
                                                    'Lamborghini Aventador LP750-4 SV'
                                                ])
    end
  end

  describe 'predicted_fastest' do
    it 'should return the list of cars that are both fastest 0-to-60 and have the best power-to-weight ratio' do
      expect(predicted_fastest @cars).to match_array([
                                                         'Bugatti Chiron',
                                                         'Ferrari 812 Superfast',
                                                         'Lamborghini Aventador LP750-4 SV',
                                                         'Porsche 918 Spyder'
                                                     ])
    end
  end
end