# Triangle Kata

# Write a method triangle()
#
# Parameters: a, b, c
#    These represent the 3 sides of a triangle.
# Returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal

def triangle(a, b, c)
end


# Write a method is_valid?
#
# Parameters: a, b, c
#    These represent the 3 sides of a triangle.
# Returns:
#    true if these sides can form a valid triangle
#    false otherwise
#
# A triangle is valid if any 2 sides, put together, are longer than the third, for all sides.

def is_valid?(a, b, c)
end