# Palindrome Kata

# Write a method is_palindrome?
#
# Parameters: string
#     This is an input string (alphabetic characters)
# Returns:
#     true if string is a palindrome
#     false otherwise
#
# A palindrome is a string that reads the same forwards and backwards
# Examples: racecar, kayak, noon, radar
#
# Challenge: Don't use built-in String API methods to solve this.

def is_palindrome?(string)
end


