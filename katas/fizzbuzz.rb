# Fizzbuzz Kata

# Write a method fizzbuzz()
#
# Parameters: none
# Returns: n/a
#
# It iterates over the numbers 1-100
# For each number:
#   print the number itself, with the following exceptions:
#   print 'fizz' if the number is divisible by 3
#   print 'buzz' if the number is divisible by 5
#   print 'fizzbuzz' if the number is divisible by 3 and 5
def fizzbuzz()
end