# Sort Strings By Cost Kata

# Write a method sort_strings_by_cost()
#
# Parameters:
#     An array of strings (lower-case alpha chars only)
# Returns:
#     A sorted array of strings, sorted by each string's "cost" ascending
#
# A string's "cost" is the sum of the cost of each letter
# and the cost of each letter is its place in the alphabet (e.g. a = 1, z = 26)

def sort_strings_by_cost(strings)
end