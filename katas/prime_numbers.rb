# Prime Numbers Kata

# Write a method primes()
#
# Parameters: number
#     This is the maximum number it will check
# Returns: array
#     An array of all the prime numbers between 1 and the maximum number it is checking
# For example:
#    primes(10)
# Returns: [2,3,5,7]

def primes(number)
end