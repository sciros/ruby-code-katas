# Fast Cars Kata

#### setup code -- loading some cars...
require 'yaml'

class Car
  attr_accessor :make, :model, :horsepower, :zero_to_sixty, :weight
end

####

# Challenge 1:
# Write a method five_fastest_cars()
#
# Parameter: cars
#     A list of cars (already read in from YML -- see above)
# Returns: []
#     A list of 5 strings, where each string is a car's make and model,
#     with these being the five fastest cars in the list from 0 to 60 mph (lower number means faster),
#     in descending order (fastest on top).
#
# Example:
#     Given 10 cars:
#       Honda Accord with 0-60 7 sec
#       Ford Taurus with 0-60 7.5 sec
#       Nissan Maxima with 0-60 6 sec
#       Pontiac Firebird with 0-60 5.5 sec
#       Dodge Caravan with 0-60 9 sec
#       Ford GT with 0-60 3 sec
#       Koenigsegg Agera with 0-60 2.5 sec
#       Ford Probe with 0-60 8 sec
#     Returns:
#       ['Koenigsegg Agera','Ford GT','Pontiac Firebird','Nissan Maxima','Honda Accord']

def five_fastest_cars(cars)
end

# Challenge 2:
# Write a method five_highest_hp()
#
# Parameter: cars
#     A list of cars (already read in from YML -- see above)
# Returns: []
#     A list of 5 numbers, where each number is a car's horsepower,
#     with these being the five highest horsepowers in the list,
#     in descending order.

def five_highest_hp(cars)
end

# Challenge 3:
# Write a method best_power_to_weight()
#
# Parameter: cars
#     A list of cars (already read in from YML -- see above)
# Returns: []
#     A list of 5 strings, where each string is a car's make and model,
#     with these having the best power-to-weight ratio in the list (lower pounds per horsepower means better),
#     in descending order (best ratio on top).

def best_power_to_weight(cars)
end

# Challenge 4:
# Write a method predicted_fastest()
#
# Parameter: cars
#     A list of cars (already read in from YML -- see above)
# Returns: []
#     A list of strings, where each string is a car's make and model,
#     with these being cars that are in the top 5 best power-to-weight ratio (Challenge 3)
#     and also in the top 5 fastest 0 to 60 (Challenge 1)
#
# The power-to-weight ratio of a car is generally considered a good indicator of performance.
# The fewer pounds per horsepower, the better the performance.
# The more cars we find in common between the two lists (best power to weight ratio and fastest to 60),
# the more correct such a notion would seem.
#
# Five cars in common would mean power to weight ratio is a great indicator.
# Zero cars in common would bean it's a poor indicator.

def predicted_fastest(cars)
end