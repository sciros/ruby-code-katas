require 'prime'

# @Dmitry - This one was a little more complex but it gave a good excuse to wrap the code in a class, use the Prime
# class from the standard library and demonstrate the use of the Before hook in rspec (thinking about what the students
# will gain by doing it). I have to admit that I didn't know about the Prime class (I guess I missed that one) when
# I started this so I ended up going down the road of generating a list of Prime numbers by hand. For this reason,
# if we delete the helper methods here I think we should replace them with a hint to look closely at the standard library
# https://ruby-doc.org/stdlib-2.3.0/
# I have used many of the other classes from this library and still managed to miss the fact that this class existed

# The prime factors of 13195 are 5, 7, 13 and 29.
#
# Create a class that will find all the prime factors for a given number (skeleton provided)
#
#     What is the largest prime factor of the number 600851475143 ?


class PrimeFactorFinder

  def get_prime_factors(number)
    # your awesome code here
  end

  #hint: use of recursion here is an option
  def get_factors(current_number, results, primes)
    # more awesome code (optional)
  end

  def is_prime(number)
    Prime.prime? number
  end

  def is_whole_number(number)
    number % 1 == 0
  end

end
