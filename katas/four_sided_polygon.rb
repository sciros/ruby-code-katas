# 4 Sided Polygon Kata

# Write a method classify_polygon()
#
# Parameters: sides, angles
#    :sides an array of 4 side lengths (numbers)
#    :angles an array of 4 angle measures (numbers)
# Returns:
#   :square  if all sides and angles are equal
#   :rectangle if all angles are equal but not all sides
#   :rhombus if all sides are equal but not all angles
#   :parallelogram if there are 2 pairs of matching sides and 2 pairs of matching angles

def classify_polygon(sides, angles)
end
