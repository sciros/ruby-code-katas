# Roman Numeral Kata

# Write a function to_roman()
#
# Parameters: number
#    A number in decimal format (e.g. 15 = one ten and five ones)
# Returns:
#    A string representing number in Roman numerals
#
# M = 1000, D = 500, C = 100, L = 50, X = 10, V = 5, I = 1
#
# Example: to_roman(15) returns 'XV'
def to_roman(decimal)
end

# Write a function to_decimal()
#
# Parameters: string
#    A number in Roman Numeral format (e.g. 'XV' = fifteen)
# Returns: number
#    A number in decimal format corresponding to the input
#
# Example: to_decimal('XV') returns 15
def to_decimal(roman)
end
